# Coding Kata: Roman Numerals

JavaScript utility for converting integers to Roman Numerals and vice versa.

## Installation instructions

To run this app locally, you will first need [Node.js](https://nodejs.org/en/) installed.

To install package dependencies (primarily React), `cd` to the app directory and run:

```
npm install
```

## Executing the tests

From the app directory, run:

```
npm test
```

## Running the app

From the app directory, run:

```
npm start
```

This will start a local webserver at http://localhost:3000/