import React from 'react';
import ReactDOM from 'react-dom';
import {
    RomanNumeralConverter,
    RomanNumeralsToInteger,
    IntegerToRomanNumerals,
    InvalidSymbolError,
    InvalidNotationError,
    NumeralsOutOfRange
} from './RomanNumeralConverter';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RomanNumeralConverter />, div);
});

describe('Roman Numerals to Integer', () => {

    let convertNumeralsToInteger = (numerals) => {
        return new RomanNumeralsToInteger(numerals).value;
    }

    it('converts I to 1', () => {
        expect(convertNumeralsToInteger('I')).toEqual(1);
    });

    it('converts V to 5', () => {
        expect(convertNumeralsToInteger('V')).toEqual(5);
    });

    it('converts X to 10', () => {
        expect(convertNumeralsToInteger('X')).toEqual(10);
    });

    it('converts L to 50', () => {
        expect(convertNumeralsToInteger('L')).toEqual(50);
    });

    it('converts C to 100', () => {
        expect(convertNumeralsToInteger('C')).toEqual(100);
    });

    it('converts D to 500', () => {
        expect(convertNumeralsToInteger('D')).toEqual(500);
    });

    it('converts M to 1000', () => {
        expect(convertNumeralsToInteger('M')).toEqual(1000);
    });

    it('converts VI to 6', () => {
        expect(convertNumeralsToInteger('VI')).toEqual(6);
    });

    it('converts IV to 4', () => {
        expect(convertNumeralsToInteger('IV')).toEqual(4);
    });

    test('integration', () => {
        expect(convertNumeralsToInteger('MMMCMXCIX')).toEqual(3999);
        expect(convertNumeralsToInteger('IX')).toEqual(9);
        expect(convertNumeralsToInteger('XL')).toEqual(40);
        expect(convertNumeralsToInteger('XC')).toEqual(90);
        expect(convertNumeralsToInteger('CD')).toEqual(400);
        expect(convertNumeralsToInteger('CM')).toEqual(900);
        expect(convertNumeralsToInteger('MDCCCCX')).toEqual(1910);
        expect(convertNumeralsToInteger('MCMX')).toEqual(1910);
        expect(convertNumeralsToInteger('MDCDIII')).toEqual(1903);
        expect(convertNumeralsToInteger('MCMIII')).toEqual(1903);
        expect(convertNumeralsToInteger('MCMLIV')).toEqual(1954);
        expect(convertNumeralsToInteger('MCMXC')).toEqual(1990);
        expect(convertNumeralsToInteger('MMXIV')).toEqual(2014);
        expect(convertNumeralsToInteger('XIIX')).toEqual(18);
        expect(convertNumeralsToInteger('IIXX')).toEqual(18);
        expect(convertNumeralsToInteger('XVIII')).toEqual(18);
    });

    it('throws on invalid symbol', () => {
        expect(() => {convertNumeralsToInteger('A')}).toThrowError(InvalidSymbolError);
    });

    it('ignores empty input', () => {
        expect(convertNumeralsToInteger('')).toBe(null);
    })

    it('throws invalid notation on subtractive V', () => {
        expect(() => {convertNumeralsToInteger('VX')}).toThrowError(InvalidNotationError);
    });

    it('throws invalid notation on subtractive L', () => {
        expect(() => {convertNumeralsToInteger('LC')}).toThrowError(InvalidNotationError);
    });

    it('throws invalid notation on subtractive D', () => {
        expect(() => {convertNumeralsToInteger('DM')}).toThrowError(InvalidNotationError);
    });

    it('throws invalid notation on triple subtractive', () => {
        expect(() => {convertNumeralsToInteger('IIIV')}).toThrowError(InvalidNotationError);
    });

    it('throws invalid notation on mixed subtractive', () => {
        expect(() => {convertNumeralsToInteger('IXL')}).toThrowError(InvalidNotationError);
    });

    it('throws invalid notation on I before L, C, D or M', () => {
        expect(() => {convertNumeralsToInteger('IL')}).toThrowError(InvalidNotationError);
        expect(() => {convertNumeralsToInteger('IC')}).toThrowError(InvalidNotationError);
        expect(() => {convertNumeralsToInteger('ID')}).toThrowError(InvalidNotationError);
        expect(() => {convertNumeralsToInteger('IM')}).toThrowError(InvalidNotationError);
    });

    it('throws invalid notation on X before D or M', () => {
        expect(() => {convertNumeralsToInteger('XD')}).toThrowError(InvalidNotationError);
        expect(() => {convertNumeralsToInteger('XM')}).toThrowError(InvalidNotationError);
    });

    it('converts lowercase to uppercase', () => {
        expect(convertNumeralsToInteger('mMCcxXiI')).toBe(2222);
    })

    it('it ignores leading and trailing whitespace', () => {
        expect(convertNumeralsToInteger('   III   ')).toBe(3);
    })

    it('throws on number out of range', () => {
        expect(() => {convertNumeralsToInteger('MMMM')}).toThrowError(NumeralsOutOfRange);
    });

});

describe('Integer to Roman Numerals', () => {

    let convertIntegerToNumerals = (value) => {
        return new IntegerToRomanNumerals(value).numerals;
    }

    it('converts 1 to I', () => {
        expect(convertIntegerToNumerals(1)).toEqual('I');
    });

    it('converts 5 to V', () => {
        expect(convertIntegerToNumerals(5)).toEqual('V');
    });

    it('converts X to 10', () => {
        expect(convertIntegerToNumerals(10)).toEqual('X');
    });

    it('converts 50 to L', () => {
        expect(convertIntegerToNumerals(50)).toEqual('L');
    });

    it('converts 100 to C', () => {
        expect(convertIntegerToNumerals(100)).toEqual('C');
    });

    it('converts 500 to D', () => {
        expect(convertIntegerToNumerals(500)).toEqual('D');
    });

    it('converts 1000 to M', () => {
        expect(convertIntegerToNumerals(1000)).toEqual('M');
    });

    it('converts 6 to VI', () => {
        expect(convertIntegerToNumerals(6)).toEqual('VI');
    });

    it('converts 4 to IV', () => {
        expect(convertIntegerToNumerals(4)).toEqual('IV');
    });

    test('integration', () => {
        expect(convertIntegerToNumerals(3999)).toEqual('MMMCMXCIX');
        expect(convertIntegerToNumerals(9)).toEqual('IX');
        expect(convertIntegerToNumerals(40)).toEqual('XL');
        expect(convertIntegerToNumerals(90)).toEqual('XC');
        expect(convertIntegerToNumerals(400)).toEqual('CD');
        expect(convertIntegerToNumerals(900)).toEqual('CM');
        expect(convertIntegerToNumerals(1910)).toEqual('MCMX');
        expect(convertIntegerToNumerals(1903)).toEqual('MCMIII');
        expect(convertIntegerToNumerals(1954)).toEqual('MCMLIV');
        expect(convertIntegerToNumerals(1990)).toEqual('MCMXC');
        expect(convertIntegerToNumerals(2014)).toEqual('MMXIV');
        expect(convertIntegerToNumerals(18)).toEqual('XVIII');
    });

    it('throws on number out of range', () => {
        expect(() => {convertIntegerToNumerals(0)}).toThrowError(NumeralsOutOfRange);
        expect(() => {convertIntegerToNumerals(4000)}).toThrowError(NumeralsOutOfRange);
    });

});
