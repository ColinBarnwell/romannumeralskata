import React, { Component } from 'react';
import './RomanNumeralConverter.css';

class InvalidSymbolError extends Error {}

class InvalidNotationError extends Error {}

class NumeralsOutOfRange extends Error {}

class RomanNumeralsToInteger {
    SYMBOL_VALUES = {
        I: 1,
        V: 5,
        X: 10,
        L: 50,
        C: 100,
        D: 500,
        M: 1000
    }
    MAX_VALUE = 3999;
    constructor(numerals) {
        this.numerals = numerals.trim().toUpperCase().split('');
        this._highestSeenSymbolValue = 0;
        this._currentAdditiveSymbol = null;
        this._currentSubtractiveSymbol = null;
        this._consecutiveSubtractiveSymbols = 0;
        this.value = this.convert();
    }
    convert() {
        let value = 0;
        this.numerals.reverse().forEach(symbol => value += this.getSignedValueOfSymbol(symbol));
        if (value > this.MAX_VALUE)
            throw new NumeralsOutOfRange("Only numbers from 1 to " + this.MAX_RANGE + " are supported.");
        return value ? value : null;
    }
    getSignedValueOfSymbol(symbol) {
        const value = this.getAbsoluteValueOfSymbol(symbol);
        const isSubtractiveSymbol = value < this._highestSeenSymbolValue;
        if (isSubtractiveSymbol) {
            this.validateSubtractiveSymbol(symbol);
            this.storeSubtractiveSymbol(symbol);
            return -value;
        } else {
            this.resetSubtractiveSymbols();
            this._currentAdditiveSymbol = symbol;
            this._highestSeenSymbolValue = value;
            return value;
        }
    }
    getAbsoluteValueOfSymbol(symbol) {
        if (symbol in this.SYMBOL_VALUES) return this.SYMBOL_VALUES[symbol];
        throw new InvalidSymbolError("'" + symbol + "' is not a valid Roman numeral.");
    }
    storeSubtractiveSymbol(symbol) {
        this._currentSubtractiveSymbol = symbol;
        this._consecutiveSubtractiveSymbols += 1;
    }
    validateSubtractiveSymbol(symbol) {
        if (symbol !== 'I' && symbol !== 'X' && symbol !== 'C')
            throw new InvalidNotationError("'" + symbol + "' cannot be used as a subtractive numeral.");
        if (symbol === 'I' && (this._currentAdditiveSymbol !== 'V' && this._currentAdditiveSymbol !== 'X'))
            throw new InvalidNotationError("I may only be subtracted from V or X.");
        else if (symbol === 'X' && (this._currentAdditiveSymbol !== 'L' && this._currentAdditiveSymbol !== 'C'))
            throw new InvalidNotationError("X may only be subtracted from L or C.");
        if (this._currentSubtractiveSymbol && this._currentSubtractiveSymbol !== symbol)
            throw new InvalidNotationError("Cannot mix different subtractive symbols together.");
        if (this._consecutiveSubtractiveSymbols >= 2)
            throw new InvalidNotationError("Cannot use more than two subtractive numerals in a row.");
    }
    resetSubtractiveSymbols() {
        this._consecutiveSubtractiveSymbols = 0;
        this._currentSubtractiveSymbol = null;
    }
}

class IntegerToRomanNumerals {
    VALUE_NOTATIONS = [
        [1000, 'M'],
        [900, 'CM'],
        [500, 'D'],
        [400, 'CD'],
        [100, 'C'],
        [90, 'XC'],
        [50, 'L'],
        [40, 'XL'],
        [10, 'X'],
        [9, 'IX'],
        [5, 'V'],
        [4, 'IV'],
        [1, 'I']
    ]
    MIN_VALUE = 1;
    MAX_VALUE = 3999;
    constructor(value) {
        if (value < this.MIN_VALUE || value > this.MAX_VALUE)
            throw new NumeralsOutOfRange("Only numbers from " + this.MIN_VALUE + " to " + this.MAX_VALUE + " are supported.")
        this.value = value;
        this.numerals = this.convert();
    }
    convert() {
        let numerals = '';
        let remainingValue = this.value;
        while (remainingValue > 0) {
            const valueNotation = this.getBiggestNotation(remainingValue);
            remainingValue -= valueNotation.value;
            numerals += valueNotation.notation;
        }
        return numerals;
    }
    getBiggestNotation(value) {
        for (let i = 0; i < this.VALUE_NOTATIONS.length; i++) {
            const notationValue = this.VALUE_NOTATIONS[i][0];
            const notation = this.VALUE_NOTATIONS[i][1];
            if (value >= notationValue) {
                return {
                    value: notationValue,
                    notation: notation
                }
            }
        }
    }
}

class RomanNumeralInput extends Component {
    constructor() {
        super();
        this.state = {displayValue: ""};
    }
    render() {
        return (
            <section className="ConversionArea RomanNumeralInput">
                <h1>Enter Roman numerals to see decimal value</h1>
                <input className="ConversionAreaInput" onChange={(event) => this.updateDisplay(event.target.value)} />
                <br />
                <span className="ConversionAreaOutput">{this.state.displayValue}</span>
            </section>
        );
    }
    updateDisplay(input) {
        let displayValue;
        try {
            displayValue = new RomanNumeralsToInteger(input).value;
        }
        catch (err) {
            displayValue = err.message;
        }
        this.setState({displayValue: displayValue});
    }
}

class IntegerInput extends Component {
    constructor() {
        super();
        this.state = {displayValue: ""};
    }
    render() {
        return (
            <section className="ConversionArea IntegerInput">
                <h1>Enter decimal values to see Roman numerals</h1>
                <input className="ConversionAreaInput" onChange={(event) => this.update(event.target.value)} />
                <br />
                <span className="ConversionAreaOutput">{this.state.displayValue}</span>
            </section>
        );
    }
    update(input) {
        let displayValue;
        let number = parseInt(input, 10);
        if (input === "") {
            displayValue = "";
        }
        else if (isNaN(number)) {
            displayValue = "Please enter an integer value."
        } else  {
            try {
                displayValue = new IntegerToRomanNumerals(number).numerals;
            }
            catch (err) {
                displayValue = err.message;
            }
        }
        this.setState({displayValue: displayValue});
    }
}

class RomanNumeralConverter extends Component {
    render() {
        return (
            <div className="RomanNumeralConverter" >
                <RomanNumeralInput />
                <IntegerInput />
            </div>
        );
    }
}

export {
    RomanNumeralConverter,
    RomanNumeralsToInteger,
    IntegerToRomanNumerals,
    InvalidSymbolError,
    InvalidNotationError,
    NumeralsOutOfRange
};
export default RomanNumeralConverter;
