import React from 'react';
import ReactDOM from 'react-dom';
import RomanNumeralConverter from './RomanNumeralConverter';
import './index.css';

ReactDOM.render(
  <RomanNumeralConverter />,
  document.getElementById('root')
);
